package org.bitbucket.kkspeed.payday2toolbox;

import java.util.ArrayList;

import android.support.v7.app.ActionBarActivity;
import android.widget.GridLayout;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class DiamondPuzzle extends ActionBarActivity {

	private final int GRID_HEIGHT = 9;
	private final int GRID_WIDTH = 6;
	private ArrayList<ArrayList<ImageButton>> btns = new ArrayList<ArrayList<ImageButton>>();
	
	private OnClickListener mTileClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			ImageButton btn = (ImageButton)v;
			Integer tag = (Integer) btn.getTag();
			
			if (tag == R.drawable.tile_default) {
				btn.setImageResource(R.drawable.tile_enabled);
				btn.setTag((Object)R.drawable.tile_enabled);
			} else {
				btn.setImageResource(R.drawable.tile_default);
				btn.setTag((Object)R.drawable.tile_default);
			}
		}
	};
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_diamond_puzzle);
		
		GridLayout grid = (GridLayout) findViewById(R.id.puzzle_board);
		
		for (int j=0; j<GRID_HEIGHT; j++) {
			ArrayList<ImageButton> btnRow = new ArrayList<ImageButton>();
			
			for (int i=0; i<GRID_WIDTH; i++) {
				ImageButton btn = new ImageButton(this);
				btn.setImageResource(R.drawable.tile_default);
				btn.setTag((Object) R.drawable.tile_default);
			    btn.setOnClickListener(mTileClickListener);
			    btn.setBackgroundColor(Color.TRANSPARENT);
			    btnRow.add(btn);
			    grid.addView(btn);
			}
			
			btns.add(btnRow);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if (HeistMenuItems.dispathItems(item, this)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void puzzleReset(View view) {
		for (ArrayList<ImageButton> row : btns) {
			for (ImageButton b : row) {
				b.setTag(R.drawable.tile_default);
				b.setImageResource(R.drawable.tile_default);
			}
		}
	}
}
