package org.bitbucket.kkspeed.payday2toolbox;

import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;

public class HeistMenuItems {
	static boolean dispathItems(MenuItem item, Activity act) {
		int id = item.getItemId();
		
		Intent intent = null;
		switch (id) {
		case R.id.action_bigoil:
			intent = new Intent(act, MainActivity.class);
			break;
		case R.id.action_diamond:
			intent = new Intent(act, DiamondPuzzle.class);
			break;
		case R.id.action_miami:
			intent = new Intent(act, MiamiAddress.class);
			break;
		case R.id.action_hoxton:
			intent = new Intent(act, HoxtonChecklist.class);
			break;
		}
		
		if (intent != null) {
			act.startActivity(intent);
			return true;
		}

		return false;
	}
}
