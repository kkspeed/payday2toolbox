package org.bitbucket.kkspeed.payday2toolbox;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainActivity extends ActionBarActivity {
	private final int HOSE_1H = 1;
	private final int HOSE_2H = 2;
	private final int HOSE_3H = 3;
	private final int ELEM_DEUT = 1;
	private final int ELEM_HYDRO = 2;
	private final int ELEM_NITRO = 3;
	private final int ELEM_UNKNOWN = 0;
	private final int HOSE_UNKNOWN = 0;
	private final int PSI_G5783 = 1;
	private final int PSI_L5812 = 2;
	private final int PSI_UNKNOWN = 0;
	private final int NUM_ENGINES = 12;
	
	private int mElem = ELEM_UNKNOWN;
	private int mHoses = HOSE_UNKNOWN;
	private int mPsi = PSI_UNKNOWN;
	
	private HashSet<Integer> possibles = new HashSet<Integer>();
	private ArrayList<HashSet<Integer>> elemPossibles = new ArrayList<HashSet<Integer>>();
	private ArrayList<HashSet<Integer>> psiPossibles = new ArrayList<HashSet<Integer>>();
	private ArrayList<HashSet<Integer>> hosePossibles = new ArrayList<HashSet<Integer>>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		for (int i=0; i<NUM_ENGINES; i++) {
			possibles.add(i);	
		}
		
		// elem unknown
		elemPossibles.add(possibles);
		
		// elem deuterium
		HashSet<Integer> tmp = new HashSet<Integer>();
		tmp.add(5 - 1);
		tmp.add(2 - 1);
		tmp.add(12 - 1);
		tmp.add(9 - 1);
		elemPossibles.add(tmp);
		
		// elem hydro
		tmp = new HashSet<Integer>();
		tmp.add(3 - 1);
		tmp.add(6 - 1);
		tmp.add(7 - 1);
		tmp.add(10 - 1);
		elemPossibles.add(tmp);
		
		// elem nitro
		tmp = new HashSet<Integer>();
		tmp.add(1 - 1);
		tmp.add(4 - 1);
		tmp.add(11 - 1);
		tmp.add(8 - 1);
		elemPossibles.add(tmp);

		// psi unknown		
		psiPossibles.add(possibles);

		// psi >= 5783
		tmp = new HashSet<Integer>();
		tmp.add(12 - 1);
		tmp.add(10 - 1);
		tmp.add(11 - 1);
		tmp.add(2 - 1);
		tmp.add(4 - 1);
		tmp.add(3 - 1);
		tmp.add(6 - 1);
		psiPossibles.add(tmp);
		
		// psi < 5812
		tmp = new HashSet<Integer>();
		tmp.add(1 - 1);
		tmp.add(3 - 1);
		tmp.add(4 - 1);
		tmp.add(5 - 1);
		tmp.add(7 - 1);
		tmp.add(9 - 1);
		tmp.add(10 - 1);
		tmp.add(8 - 1);
		psiPossibles.add(tmp);
		
		// hose unknown 
		hosePossibles.add(possibles);
		// hose 1H
		tmp = new HashSet<Integer>();
		tmp.add(1 - 1);
		tmp.add(2 - 1);
		hosePossibles.add(tmp);
		
		// hose 2H
		tmp = new HashSet<Integer>();
		for (int i=3; i<=6; i++) {
			tmp.add(i - 1);
		}
		hosePossibles.add(tmp);
		
		// hose 3H
		tmp = new HashSet<Integer>();
		for (int i=7; i<=12; i++) {
			tmp.add(i - 1);
		}
		hosePossibles.add(tmp);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if (HeistMenuItems.dispathItems(item, this)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void elemButtonDeutClicked(View view) {
		mElem = ELEM_DEUT;
		updateElements();
	}
	
	public void elemButtonHydroClicked(View view) {
		mElem = ELEM_HYDRO;
		updateElements();
	}
	
	public void elemButtonNitroClicked(View view) {
		mElem = ELEM_NITRO;
		updateElements();
	}
	
	public void g5783Clicked(View view) {
		mPsi = PSI_G5783;
		updatePsi();
	}
	
	public void l5812Clicked(View view) {
		mPsi = PSI_L5812;
		updatePsi();
	}
	
	public void hoseButton3HClicked(View view) {
		mHoses = HOSE_3H;
		updateHoses();
	}
	
	public void hoseButton2HClicked(View view) {
		mHoses = HOSE_2H;
		updateHoses();
	}
	
	public void hoseButton1HClicked(View view) {
		mHoses = HOSE_1H;
		updateHoses();
	}
	
	public void resetButtonClicked(View view) {
		mHoses = HOSE_UNKNOWN;
		mPsi = PSI_UNKNOWN;
		mElem = ELEM_UNKNOWN;
		
		updateHoses();
		updateElements();
		updatePsi();
	}
	
	public void updateHoses() {
		ImageButton btn3H = (ImageButton) findViewById(R.id.hoseButton3H);
		ImageButton btn2H = (ImageButton) findViewById(R.id.hoseButton2H);
		ImageButton btn1H = (ImageButton) findViewById(R.id.hoseButton1H);
		
		btn1H.setImageResource(R.drawable.button_1h);
		btn2H.setImageResource(R.drawable.button_2h);
		btn3H.setImageResource(R.drawable.button_3h);
		
		switch(mHoses) {
		case HOSE_3H:
			btn3H.setImageResource(R.drawable.button_3h_selected);
			break;
		case HOSE_2H:
			btn2H.setImageResource(R.drawable.button_2h_selected);
			break;
		case HOSE_1H:
			btn1H.setImageResource(R.drawable.button_1h_selected);
			break;
		}
		
		showEngines();
	}
	
	public void updateElements() {
		ImageButton btnHydro = (ImageButton) findViewById(R.id.elemHydro);
		ImageButton btnNitro = (ImageButton) findViewById(R.id.elemNitro);
		ImageButton btnDeut = (ImageButton) findViewById(R.id.elemDeut);
		
		btnHydro.setImageResource(R.drawable.hydro);
		btnNitro.setImageResource(R.drawable.nitro);
		btnDeut.setImageResource(R.drawable.deut);
		
		switch(mElem) {
		case ELEM_HYDRO:
			btnHydro.setImageResource(R.drawable.hydro_selected);
			break;
		case ELEM_NITRO:
			btnNitro.setImageResource(R.drawable.nitro_selected);
			break;
		case ELEM_DEUT:
			btnDeut.setImageResource(R.drawable.deut_selected);
			break;
		}
		
		showEngines();
	}
	
	public void updatePsi() {
		ImageButton btnG5783 = (ImageButton) findViewById(R.id.g5783);
		ImageButton btnL5812 = (ImageButton) findViewById(R.id.l5812);
		
		btnG5783.setImageResource(R.drawable.g5783);
		btnL5812.setImageResource(R.drawable.l5812);
		
		switch(mPsi) {
		case PSI_G5783:
			btnG5783.setImageResource(R.drawable.g5783_selected);
			break;
		case PSI_L5812:
			btnL5812.setImageResource(R.drawable.l5812_selected);
			break;
		}
		
		showEngines();
	}
	
	public void showEngines() {
		ImageView engine1 = (ImageView) findViewById(R.id.engine1);
		ImageView engine2 = (ImageView) findViewById(R.id.engine2);
		ImageView engine3 = (ImageView) findViewById(R.id.engine3);
		ImageView engine4 = (ImageView) findViewById(R.id.engine4);
		ImageView engine5 = (ImageView) findViewById(R.id.engine5);
		ImageView engine6 = (ImageView) findViewById(R.id.engine6);
		ImageView engine7 = (ImageView) findViewById(R.id.engine7);
		ImageView engine8 = (ImageView) findViewById(R.id.engine8);
		ImageView engine9 = (ImageView) findViewById(R.id.engine9);
		ImageView engine10 = (ImageView) findViewById(R.id.engine10);
		ImageView engine11 = (ImageView) findViewById(R.id.engine11);
		ImageView engine12 = (ImageView) findViewById(R.id.engine12);
		
		engine1.setImageResource(R.drawable.engine1);
		engine2.setImageResource(R.drawable.engine2);
		engine3.setImageResource(R.drawable.engine3);
		engine4.setImageResource(R.drawable.engine4);
		engine5.setImageResource(R.drawable.engine5);
		engine6.setImageResource(R.drawable.engine6);
		engine7.setImageResource(R.drawable.engine7);
		engine8.setImageResource(R.drawable.engine8);
		engine9.setImageResource(R.drawable.engine9);
		engine10.setImageResource(R.drawable.engine10);
		engine11.setImageResource(R.drawable.engine11);
		engine12.setImageResource(R.drawable.engine12);
		
		ArrayList<ImageView> engines = new ArrayList<ImageView>();
		engines.add(engine1);
		engines.add(engine2);
		engines.add(engine3);
		engines.add(engine4);
		engines.add(engine5);
		engines.add(engine6);
		engines.add(engine7);
		engines.add(engine8);
		engines.add(engine9);
		engines.add(engine10);
		engines.add(engine11);
		engines.add(engine12);
		
		for (int i=0; i<NUM_ENGINES; i++) {
			if (!(hosePossibles.get(mHoses).contains(i) &&
			      elemPossibles.get(mElem).contains(i) &&
			      psiPossibles.get(mPsi).contains(i))) {
				engines.get(i).setImageResource(R.drawable.engine_no);
			}
		}
	}
}
