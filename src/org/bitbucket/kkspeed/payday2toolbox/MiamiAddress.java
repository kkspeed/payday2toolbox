package org.bitbucket.kkspeed.payday2toolbox;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MiamiAddress extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_miami_address);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if (HeistMenuItems.dispathItems(item, this)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void btnHlmAddressClicked(View view) {
		ImageButton current = (ImageButton) view;
		ImageButton btnWestend = (ImageButton) findViewById(R.id.btn_westend);
		ImageButton btnDowntown = (ImageButton) findViewById(R.id.btn_downtown);
		ImageButton btnFoggybottom = (ImageButton) findViewById(R.id.btn_foggybottom);
		ImageButton btnShaw = (ImageButton) findViewById(R.id.btn_shaw);
		ImageButton btnGeorgetown = (ImageButton) findViewById(R.id.btn_georgetown);

		btnWestend.setImageResource(R.drawable.btn_westend);
		btnDowntown.setImageResource(R.drawable.btn_downtown);
		btnFoggybottom.setImageResource(R.drawable.btn_foggy_bottom);
		btnShaw.setImageResource(R.drawable.btn_shaw);
		btnGeorgetown.setImageResource(R.drawable.btn_georgetown);
		
		ImageView hlmAddressPic = (ImageView)findViewById(R.id.hlm_address_picture);
		
		switch(current.getId()) {
		case R.id.btn_westend:
			current.setImageResource(R.drawable.btn_westend_selected);
			hlmAddressPic.setImageResource(R.drawable.hlm_westend);
			break;
		case R.id.btn_foggybottom:
			current.setImageResource(R.drawable.btn_foggy_bottom_selected);
			hlmAddressPic.setImageResource(R.drawable.hlm_foggybottom);
			break;
		case R.id.btn_downtown:
			current.setImageResource(R.drawable.btn_downtown_selected);
			hlmAddressPic.setImageResource(R.drawable.hlm_downtown);
			break;
		case R.id.btn_shaw:
			current.setImageResource(R.drawable.btn_shaw_selected);
			hlmAddressPic.setImageResource(R.drawable.hlm_shaw);
			break;
		case R.id.btn_georgetown:
			current.setImageResource(R.drawable.btn_georgetown_selected);
			hlmAddressPic.setImageResource(R.drawable.hlm_georgetown);
			break;
		}
	}
}
